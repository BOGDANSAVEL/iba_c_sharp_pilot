﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IBA_Pilot
{
    class Player
    {
        public string Name { get; set; }
        public double Score { get; set; }
    }
}
