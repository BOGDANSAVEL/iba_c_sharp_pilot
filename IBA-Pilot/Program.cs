﻿using IBA_Pilot.Languages;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IBA_Pilot
{
    class Program
    {
        static async Task Main(string[] args)
        {
            JsonListParser<Language> languagesParser = new JsonListParser<Language>("../../../Languages/Languages.json");
            IList<Language> languages = await languagesParser.ReadAsync();
            Game game = new Game(languages);
            await game.PlayAsync();
        }
    }
}
