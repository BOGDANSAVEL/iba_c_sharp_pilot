﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IBA_Pilot.Languages
{
    class Language
    {
        public string Name { get; set; }
        public int EncodingCode { get; set; }

        public string RegEx { get; set; }

        public IDictionary<string, string> Messages { get; set; }

        public Language()
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
        }

        public void WriteLine(string line)
        {
            Console.WriteLine(Encoding.GetEncoding(EncodingCode).GetString(Encoding.GetEncoding(EncodingCode).GetBytes(line)));
        }
    }
}
