﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IBA_Pilot.Exceptions
{
    class WordCheckingException : Exception
    {
        public WordCheckingException(string message)
        : base(message)
        { }
    }
}
