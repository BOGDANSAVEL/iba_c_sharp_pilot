﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IBA_Pilot.Exceptions
{
    class WordHaveBeenUsedException : WordCheckingException
    {
        public WordHaveBeenUsedException(string message)
        : base(message)
        { }
    }
}
