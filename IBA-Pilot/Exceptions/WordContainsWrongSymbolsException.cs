﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IBA_Pilot.Exceptions
{
    class WordContainsWrongSymbolsException : WordCheckingException
    {

        public WordContainsWrongSymbolsException(string message)
        : base(message)
        { }
    }
}
