﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IBA_Pilot.Exceptions
{
    class WrongWordSizeException : WordCheckingException
    {
        public WrongWordSizeException(string message)
        : base(message)
        { }
    }
}
