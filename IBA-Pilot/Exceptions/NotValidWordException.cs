﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IBA_Pilot.Exceptions
{
    class NotValidWordException : WordCheckingException
    { 

        public NotValidWordException(string message)
        : base(message)
        { }
    }
}
