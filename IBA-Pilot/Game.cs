﻿using IBA_Pilot.Exceptions;
using IBA_Pilot.Languages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Timers;

namespace IBA_Pilot
{
    class Game
    {
        private const double ONE_MINUTE = 60000;
        private const double TIME = ONE_MINUTE;
        private const string CHOOSE_LANGUAGE_MESSAGE = "Choose language: ";
        private const string WRITE_LANGUEAGE_NUMBER = "Write down number 1 - {0}:";

        public IList<Player> Players { get; set; }
        public IList<Language> Languages { get; set; }

        public Language GameLanguage { get; set; }

        private IList<string> _words = new List<string>();
        private bool _gameover = false;
        private IEnumerator<Player> _enumerator;
        private PlayersRegistrar _playersRegistrar;

        public Game(IList<Language> languages)
        {
            Languages = languages;
        }

        public async Task PlayAsync()
        {
            GameLanguage = ChooseLanguage();
            _playersRegistrar = new PlayersRegistrar(GameLanguage);
            Players = await _playersRegistrar.RegistratePlayers();
            _enumerator = Players.GetEnumerator();
            Player activePlayer = GetNextPlayer();
            AppDomain.CurrentDomain.ProcessExit += new EventHandler((sender, e) => RageQuit());
            GameLanguage.WriteLine(GameLanguage.Messages["GAME_BEGINS_MESSAGE"]);
            GameLanguage.WriteLine(string.Format(GameLanguage.Messages["WRITE_FIRST_WORD_MESSAGE"], activePlayer.Name));
            string firstWord = ReadFirstWord();
            _words.Add(firstWord);
            GameLanguage.WriteLine(string.Format(GameLanguage.Messages["TIME_LIMIT_MESSAGE"], TIME / ONE_MINUTE));

            Timer aTimer = new Timer(TIME);
            aTimer.Elapsed += (sender, e) => EndGame();
            aTimer.AutoReset = false;
            try
            {
                while (!_gameover)
                {
                    activePlayer = GetNextPlayer();
                    aTimer.Start();
                    GameLanguage.WriteLine(string.Format(GameLanguage.Messages["WRITE_THE_WORD_MESSAGE"], activePlayer.Name));
                    string nextWord = ReadNextWord();
                    _words.Add(nextWord);
                    aTimer.Stop();
                }
            } 
            catch (GameOverException)
            {
                aTimer.Dispose();
                await _playersRegistrar.SavePlayersAsync();
            }
        }

        private Language ChooseLanguage()
        {
            Console.WriteLine(CHOOSE_LANGUAGE_MESSAGE);
            int i = 0;
            foreach (Language language in Languages)
            {
                Console.WriteLine(string.Format("{0}. {1}", ++i, language.Name));
            }
            return Languages[ReadLangueageNumber(i)-1];            
        }

        private int ReadLangueageNumber(int i)
        {
            int number = 0;
            Console.WriteLine(string.Format(WRITE_LANGUEAGE_NUMBER, i));
            try
            {
                number = int.Parse(Console.ReadLine());
            } catch (FormatException)
            {
                number = ReadLangueageNumber(i);
            }
            if (!(number <= i && number > 0))
            {
                number = ReadLangueageNumber(i);
            }
            return number;
        }

        public void RageQuit()
        {
            if (!_gameover)
            {
                GetNextPlayer().Score++;
                _playersRegistrar.SavePlayersAsync().Wait();
            }
        }

        private Player GetNextPlayer()
        {
            if (!_enumerator.MoveNext())
            {
                _enumerator = Players.GetEnumerator();
                _enumerator.MoveNext();
            }
            return _enumerator.Current;
        }

        private void EndGame()
        {
            Player winner = GetNextPlayer();
            winner.Score++;
            GameLanguage.WriteLine(string.Format(GameLanguage.Messages["GAME_OVER_MESSAGE"], winner.Name));
            GameLanguage.WriteLine(GameLanguage.Messages["ACTION_TO_CLOSE_THE_GAME"]);
            _gameover = true;
        }

        private string ReadNextWord()
        {
            string nextWord = Console.ReadLine();
            while(CheckIfCommand(nextWord))
            {
                RunCommand(nextWord);
                GameLanguage.WriteLine(string.Format(GameLanguage.Messages["WRITE_THE_WORD_MESSAGE"], _enumerator.Current.Name));
                nextWord = Console.ReadLine();
            }
            try
            {
                if (_gameover)
                {
                    throw new GameOverException();
                }
                WordChecker wordChecker = new WordChecker(GameLanguage);
                wordChecker.CheckIfContainsOnlyLetters(nextWord);
                wordChecker.CheckIfHaveBeenUsed(_words, nextWord);
                wordChecker.CheckIfValid(_words[0], nextWord);
            } 
            catch (WordCheckingException ex)
            {
                GameLanguage.WriteLine(ex.Message);
                GameLanguage.WriteLine(GameLanguage.Messages["TRY_ANOTHER_WORD_MESSAGE"]);
                nextWord = ReadNextWord();
            }
            return nextWord;
        }

        private string ReadFirstWord()
        {
            string firstWord = Console.ReadLine();
            while (CheckIfCommand(firstWord))
            {
                RunCommand(firstWord);
                GameLanguage.WriteLine(string.Format(GameLanguage.Messages["WRITE_THE_WORD_MESSAGE"], _enumerator.Current.Name));
                firstWord = Console.ReadLine();
            }
            try
            {
                WordChecker wordChecker = new WordChecker(GameLanguage);
                wordChecker.CheckIfContainsOnlyLetters(firstWord);
                wordChecker.CheckFirstWordSize(firstWord);
            }
            catch (WordCheckingException ex)
            {
                GameLanguage.WriteLine(ex.Message);
                GameLanguage.WriteLine(GameLanguage.Messages["TRY_ANOTHER_WORD_MESSAGE"]);
                firstWord = ReadFirstWord();
            }
            return firstWord;
        }

        private bool CheckIfCommand(string word)
        {
            if (word.Length > 0 && word[0] == '/')
            {
                return true;
            } else
            {
                return false;
            }
        }

        private void RunCommand(string command)
        {
            switch (command)
            {
                case "/show-words":
                    GameLanguage.WriteLine(GameLanguage.Messages["SHOW_WORDS_COMMAND_MESSAGE"]);
                    foreach (string word in _words)
                    {
                        GameLanguage.WriteLine(word);
                    }
                    break;
                case "/score":
                    GameLanguage.WriteLine(GameLanguage.Messages["SCORE_COMMAND_MESSAGE"]);
                    foreach (Player player in Players)
                    {
                        GameLanguage.WriteLine($"{player.Name}: {player.Score}");
                    }
                    break;
                case "/total-score":
                    GameLanguage.WriteLine(GameLanguage.Messages["TOTAL_SCORE_COMMAND_MESSAGE"]);
                    foreach (Player player in _playersRegistrar.Players)
                    {
                        GameLanguage.WriteLine($"{player.Name}: {player.Score}");
                    }
                    break;
                default:
                    GameLanguage.WriteLine(GameLanguage.Messages["WRONG_COMMAND_MESSAGE"]);
                    break;
            }
        }
    }
}
