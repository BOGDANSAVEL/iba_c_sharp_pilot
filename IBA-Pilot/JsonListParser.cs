﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace IBA_Pilot
{
    class JsonListParser<T>
    {
        private readonly string _jsonFileName;

        public JsonListParser(string jsonFileName)
        {
            this._jsonFileName = jsonFileName;
        }

        public async Task<IList<T>> ReadAsync()
        {
            try
            {
                using (FileStream fs = new FileStream(_jsonFileName, FileMode.OpenOrCreate))
                {
                    return await JsonSerializer.DeserializeAsync<IList<T>>(fs);
                }
            }
            catch (JsonException)
            {
                return new List<T>();
            }
        }

        public async Task WriteAsync(IList<T> listToWrite)
        {
            using (FileStream fs = new FileStream(_jsonFileName, FileMode.OpenOrCreate))
            {
                await JsonSerializer.SerializeAsync(fs, listToWrite);
            }
        }
    }
}
