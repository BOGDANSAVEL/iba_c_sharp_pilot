﻿using IBA_Pilot.Exceptions;
using IBA_Pilot.Languages;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace IBA_Pilot
{
    class WordChecker
    {
        private const int MIN_LENGTH = 8;
        private const int MAX_LENGTH = 30;

        public Language Langueage { get; set; }

        public WordChecker(Language gameLanguage)
        {
            Langueage = gameLanguage;
        }

        public void CheckIfContainsOnlyLetters(string word)
        {
            if (!Regex.IsMatch(word, @Langueage.RegEx))
            {
                throw new WordContainsWrongSymbolsException(Langueage.Messages["WORD_CONTAINS_WRONG_SYMBOLS_EXCEPTION_MESSAGE"]);
            }
        }

        public void CheckIfHaveBeenUsed(IList<string> words, string word)
        {
            if (words.Contains(word))
            {
                throw new WordHaveBeenUsedException(Langueage.Messages["WORD_HAVE_BEEN_USED_EXCEPTION_MESSAGE"]);
            }
        }

        public void CheckFirstWordSize(string word)
        {
            if (word.Length > MAX_LENGTH || word.Length < MIN_LENGTH)
            {
                throw new WrongWordSizeException(string.Format(Langueage.Messages["WRONG_WORD_SIZE_ECXEPTION_MESSAGE"], MIN_LENGTH, MAX_LENGTH));
            }
        }

        public void CheckIfValid(string firstWord, string word)
        {
            if(!IsValid(firstWord, word))
            {
                throw new NotValidWordException(Langueage.Messages["NOT_VALID_WORD_EXCEPTION_MESSAGE"]);
            }
        }

        public bool IsValid(string firstWord, string word)
        {
            Dictionary<char, int> firstWordDict = CreateDictionaryForWord(firstWord);
            Dictionary<char, int> wordDict = CreateDictionaryForWord(word);
            foreach (KeyValuePair<char, int> letter in wordDict)
            {
                if (!firstWordDict.ContainsKey(letter.Key) || firstWordDict[letter.Key] < letter.Value)
                {
                    return false;
                }
            }
            return true;
        }

        private Dictionary<char, int> CreateDictionaryForWord(string word)
        {
            Dictionary<char, int> wordDict = new Dictionary<char, int>();
            for (int i = 0; i < word.Length; i++)
            {
                int value;
                if (wordDict.TryGetValue(word[i], out value))
                {
                    wordDict[word[i]] = value + 1;
                }
                else
                {
                    wordDict.Add(word[i], 1);
                }
            }
            return wordDict;
        }
    }
}
