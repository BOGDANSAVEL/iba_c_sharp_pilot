﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Text.Json;
using System.Linq;
using IBA_Pilot.Languages;

namespace IBA_Pilot
{
    class PlayersRegistrar
    {
        private const string JSON_FILE_NAME = "games.json";
        private const int MIN_LENGTH = 6;
        private const int MAX_LENGTH = 30;
        private Language _GameLanguage { get; set; }
        private JsonListParser<Player> _PlayersParser { get; set; }

        public IList<Player> Players { get; set; }

        public PlayersRegistrar(Language gameLangueage)
        {
            _GameLanguage = gameLangueage;
            _PlayersParser = new JsonListParser<Player>(JSON_FILE_NAME);
        }

        public async Task<IList<Player>> RegistratePlayers()
        {
            Players = await LoadPlayersAsync();
            _GameLanguage.WriteLine(_GameLanguage.Messages["FIRST_PLAYER_LOGIN_MESSAGE"]);
            string firstPlayerName = Console.ReadLine();
            Player firstPlayer = Login(firstPlayerName);
            _GameLanguage.WriteLine(_GameLanguage.Messages["SECOND_PLAYER_LOGIN_MESSAGE"]);
            string secondPlayerName = Console.ReadLine();
            while (secondPlayerName == firstPlayer.Name)
            {
                _GameLanguage.WriteLine(string.Format(_GameLanguage.Messages["PLAYER_ALREADY_IN_GAME_ERROR_MESSSAGE"], secondPlayerName));
                secondPlayerName = Console.ReadLine();
            }
            Player secondPlayer = Login(secondPlayerName);
            await SavePlayersAsync();
            return new List<Player>() { firstPlayer, secondPlayer };
        }

        public async Task<IList<Player>> LoadPlayersAsync()
        {
            return await _PlayersParser.ReadAsync();
        }

        public async Task SavePlayersAsync()
        {
            await _PlayersParser.WriteAsync(Players);
        }

        private Player Login(string name)
        {
            Player player;
            while(!CheckLoginSize(name))
            {
                _GameLanguage.WriteLine(string.Format(_GameLanguage.Messages["WRONG_NICKNAME_SIZE_ECXEPTION_MESSAGE"], MIN_LENGTH, MAX_LENGTH));
                name = Console.ReadLine();
            }
            IEnumerable<Player> selectedPlayers = from p in Players where p.Name.Equals(name) select p;
            if (selectedPlayers.Count() > 0)
            {
                player = selectedPlayers.First();
                _GameLanguage.WriteLine(string.Format(_GameLanguage.Messages["GAME_GREETING_MESSAGE"], name, selectedPlayers.First().Score));
            }
            else
            {
                player = new Player() { Name = name, Score = 0 };
                Players.Add(player);
                _GameLanguage.WriteLine(string.Format(_GameLanguage.Messages["FIRST_GAME_GREETING_MESSAGE"], name));
            }
            return player;
        }

        public bool CheckLoginSize(string word)
        {
            if (word.Length > MAX_LENGTH || word.Length < MIN_LENGTH)
            {
                return false;
            }
            return true;
        }
    }
}
